#!/usr/bin/env bash
#############################
main(){
adduser
creat_ssh
upload_server
create_ssh_key
copy_key_to_designated_server
}
adduser(){
if [ $(id -u) -eq 0 ]; then
	read -p "Enter username : " username
	read -s -p "Enter password : " password
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$password
		useradd -m -p "$pass" "$username" && usermod -s /bin/bash "$username"
		[ $? -eq 0 ] && echo "User has been added to system!" || echo "Failed to add a user!"
	fi
else
	echo "Only root may add a user to the system."
	exit 2
fi
}

create_ssh(){
	systemctl enable --now ssh| $username@127.0.0.1

}

upload_server(){
ip link set enp0s3 up 
ip addr add 10.0.2.16/24 gw 10.0.2.2 dns "8.8.8.8" dev enp0s3
#	nmcli con down enp0s3
#	nmcli con mod enp0s3 ipv4.addresses 10.0.2.16/2 ipv4.getway 10.0.2.5 ipv4.dns "8.8.8.8" ipv.method manual
#	nmcli con up enp0s3
}
create_ssh_key(){
shh-keygen <<EOF
$username
1	
1
EOF
}
copy_key_to_designated_server(){
scp $username.pub $username@10.0.2.15:~/home/$username


}
main 
