#!/usr/bin/env bash

## this is a shared work on a script

############################################
#Authors: Dorin Davidov + Michaell Vaknin
#Purpose: menu for backup partitions
#Date: 21.10.2020
#Version: updated in GitLAb
############################################


##array and variables##
options=("backup mbr" "backup home partition" "backup and zip home partition" "backup and zip root home partition" "backup and zip entire disk" "cleanup swap" "exit")
PS3="please select option: "


##functions

function backup_mbr(){
read -p "please select disk name: " diskname

if $1; then
sudo dd if=/dev/$diskname bs=2047 count=1 of=mbr_backup.img
else
printf "%s\n" "mbr backup failed"

fi
}


function backup_home_partition(){
read -p "please select disk name: " diskname

if $1; then
sudo dd if=/dev/$diskname bs=25G of=bootsector.sd.dd count=1 
else 
printf "%s\n" "backup home partition failed"

fi
}

function backup_and_zip_home_partition(){
read -p "please select disk name: " diskname

if $1; then
sudo dd if=/dev/$diskname bs=512 | gzip -c -6 > disk.dd.gz
else 
printf "%s\n" "backup and zipping home partition failed"

fi
}

function backup_and_zip_root_home_partition(){
read -p "please select disk name: " diskname

if $1; then
sudo dd if=/dev/$diskname bs=512 | gzip -c -6 > disk.dd.gz
else 
printf "%s\n" "backup and zipping root and home partition failed"

fi
}

function backup_and_zip_entire_disk(){
read -p "please select disk name: " diskname

if $1; then
sudo dd if=/dev/$diskname bs=512 | gzip -c -6 > entire_disk.dd.gz
else 
printf "%s\n" "backup and zipping entire disk failed"

fi
}

function clear_swap(){
	swapoff -a
	sleep 45s
	swapon -a 
}


lsblk

echo $PS3

select opt in "${options[@]}"
do
	case $opt in 
		1) backup_mbr  
		printf "%s\n" "backup mbr completed" || printf "%s\n" "backup mbr faild"

		;;
		2) backup_home_partition 
	        printf "%s\n" "backup home partition completed"

	        ;;
	        3) backup_and_zip_home_partition
	        printf "%s\n" "backup and zip home partition completed"

	        ;;
		4) backup_and_zip_root_home_partition
	        printf "%s\n" "backup and zip root and home partition completed"

	        ;;
                5) backup_and_zip_entire_disk
    	        printf "%s\n" "backup and zip entire disk completed" 
	       
	        ;;
       	        6) clear_swap
	        printf "%s\n" "clear swap completed"

   	        ;;
	        *) exit

	       ;;

       esac

       done 
